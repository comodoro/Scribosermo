FROM dsbuild
WORKDIR /DeepSpeech/

# Patch the bazel config to add suppport for rpi4
COPY .bazelrc.patch /DeepSpeech/tensorflow/
RUN cd tensorflow/ && patch .bazelrc .bazelrc.patch

# Build binaries to install some requirements
RUN cd /DeepSpeech/tensorflow/ && \
  bazel build --workspace_status_command="bash native_client/bazel_workspace_status_cmd.sh" \
  --config=monolithic --config=rpi4 --config=rpi4_opt -c opt --copt=-O3 --copt=-fvisibility=hidden //native_client:libdeepspeech.so

# Get Ubuntu Focal (20.04) file tree
RUN apt-get update && apt-get install -y multistrap
COPY multistrap_ubuntu_aarch64_focal.conf native_client/
RUN multistrap -d multistrap-ubuntu-aarch64-focal -f native_client/multistrap_ubuntu_aarch64_focal.conf

# Link system libraries into host system
RUN ln -s /DeepSpeech/multistrap-ubuntu-aarch64-focal/lib/ld-linux-aarch64.so.1 /lib/
RUN ln -s /DeepSpeech/multistrap-ubuntu-aarch64-focal/lib/aarch64-linux-gnu /lib/
RUN ln -s /DeepSpeech/multistrap-ubuntu-aarch64-focal/usr/lib/aarch64-linux-gnu /usr/lib/

# Install python 3.8
RUN apt-get install -y python3.8 && ln -sf python3.8 /usr/bin/python3
 
# Install our prebuilt raspbian-swig
RUN rm -rf /DeepSpeech/native_client/ds-swig/
COPY ds-swig.tar.gz /DeepSpeech/native_client/
RUN cd /DeepSpeech/native_client/ && tar xzf ds-swig.tar.gz

# Link swig
RUN ln -s /DeepSpeech/native_client/ds-swig/bin/swig /usr/bin/
RUN mkdir -p /ds-swig/share/swig/
RUN ln -s /DeepSpeech/native_client/ds-swig/share/swig/4.0.2 /ds-swig/share/swig/
 
# Patch the definitions to add support for rpi4
COPY definitions.mk.patch /DeepSpeech/native_client/
RUN cd native_client/ && patch definitions.mk definitions.mk.patch

WORKDIR /DeepSpeech/native_client/ctcdecode
# Clean the cache and build the ctc-decoder package
RUN make clean
RUN make TARGET=rpi4 NUM_PROCESSES=$(nproc) bindings

# Rename the swigwrapper binary to _swigwrapper.so
# TODO find a cleaner solution
RUN apt-get install -y zip
RUN unzip dist/*.whl
RUN mv ds_ctcdecoder/_swigwrapper.*.so ds_ctcdecoder/_swigwrapper.so
RUN rm      dist/ds_ctcdecoder-0.10.0a3-cp38-cp38-linux_aarch64.whl \
  && zip -r dist/ds_ctcdecoder-0.10.0a3-cp38-cp38-linux_aarch64.whl ds_ctcdecoder ds_ctcdecoder-*

CMD ["/bin/bash"]
