import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_io as tfio

# ==================================================================================================


def extra_silence(signal, amount: int):
    """Append additional silence at the beginning and the end of the audio signal"""

    silence = tf.zeros([tf.shape(signal)[0], amount], signal.dtype)
    signal = tf.concat([silence, signal, silence], axis=-1)
    return signal


# ==================================================================================================


def dither(signal, factor: float):
    """Amount of additional white-noise dithering to prevent quantization artefacts"""

    signal += factor * tf.random.normal(shape=tf.shape(signal))
    return signal


# ==================================================================================================


def normalize_volume(signal):
    """Normalize volume to range [-1,1]"""

    gain = 1.0 / (tf.reduce_max(tf.abs(signal), axis=0) + 1e-7)
    signal = signal * gain
    return signal


# ==================================================================================================


def preemphasis(signal, coef=0.97):
    """Emphasizes high-frequency signal components"""

    psig = signal[:, 1:] - coef * signal[:, :-1]
    signal = tf.concat([tf.expand_dims(signal[:, 0], axis=-1), psig], axis=1)
    return signal


# ==================================================================================================


def resample(signal, sample_rate: int, tmp_sample_rate: int):
    """Resample to given sample rate and back again"""

    def resample_helper(signal):
        signal = tfio.audio.resample(signal, sample_rate, tmp_sample_rate)
        signal = tfio.audio.resample(signal, tmp_sample_rate, sample_rate)
        return signal

    # Split up into single examples
    signal = tf.map_fn(resample_helper, signal)

    return signal


# ==================================================================================================


def random_volume(signal, min_dbfs: float, max_dbfs: float):
    """Apply random volume changes.
    Inspired by: DeepSpeech/training/deepspeech_training/util/augmentations.py"""

    def rms_to_dbfs(rms):
        dbfs = tf.math.log(tf.maximum(1e-16, rms)) / tf.math.log(10.0)
        dbfs = 20.0 * dbfs + 3.0103
        return dbfs

    def gain_db_to_ratio(gain_db):
        return tf.math.pow(10.0, gain_db / 20.0)

    def get_max_dbfs(data):
        # Peak dBFS based on the maximum energy sample.
        # Will prevent overdrive if used for normalization.
        amax = tf.abs(tf.reduce_max(data))
        amin = tf.abs(tf.reduce_min(data))
        dmax = tf.maximum(amax, amin)
        dbfs_max = rms_to_dbfs(dmax)
        return dbfs_max

    def normalize(data, dbfs):
        # Change volume and clip signal range
        data = data * gain_db_to_ratio(dbfs - get_max_dbfs(data))
        data = tf.maximum(tf.minimum(data, 1.0), -1.0)
        return data

    def norm_helper(signal):
        target_dbfs = tf.random.uniform(shape=[], minval=min_dbfs, maxval=max_dbfs)
        signal = normalize(signal, target_dbfs)
        return signal

    # Split up into single examples
    signal = tf.map_fn(norm_helper, signal)

    return signal


# ==================================================================================================


def reverb(signal, audio_sample_rate: int, delay: float = 20, decay: float = 10):
    """Adds simplified (no all-pass filters) Schroeder reverberation. Very time consuming.
    Inspired by: DeepSpeech/training/deepspeech_training/util/augmentations.py"""

    def rms_to_dbfs(rms):
        dbfs = tf.math.log(tf.maximum(1e-16, rms)) / tf.math.log(10.0)
        dbfs = 20.0 * dbfs + 3.0103
        return dbfs

    def gain_db_to_ratio(gain_db):
        return tf.math.pow(10.0, gain_db / 20.0)

    def max_dbfs(data):
        # Peak dBFS based on the maximum energy sample.
        # Will prevent overdrive if used for normalization.
        amax = tf.abs(tf.reduce_max(data))
        amin = tf.abs(tf.reduce_min(data))
        dmax = tf.maximum(amax, amin)
        dbfs_max = rms_to_dbfs(dmax)
        return dbfs_max

    def normalize(data, dbfs):
        # Change volume and clip signal range
        data = data * gain_db_to_ratio(dbfs - max_dbfs(data))
        data = tf.maximum(tf.minimum(data, 1.0), -1.0)
        return data

    def reverb_helper(signal):

        # Get a random delay and decay
        r_delay = tf.random.uniform(shape=[], minval=0, maxval=delay)
        r_decay = tf.random.uniform(shape=[], minval=0, maxval=decay)

        orig_dbfs = max_dbfs(signal)
        r_decay = gain_db_to_ratio(tf.math.negative(r_decay))
        result = tf.identity(signal)
        signal_len = tf.shape(signal)[0]

        # Primes to minimize comb filter interference
        primes = [17, 19, 23, 29, 31]

        for delay_prime in primes:
            layer = tf.identity(signal)

            # 16 samples minimum to avoid performance trap and risk of division by zero
            n_delay = r_delay * (delay_prime / primes[0]) * (audio_sample_rate / 1000.0)
            n_delay = tf.cast(tf.floor(tf.maximum(16.0, n_delay)), dtype=tf.int32)

            w_range = tf.cast(tf.floor(signal_len / n_delay), dtype=tf.int32)
            for w_index in range(0, w_range):
                w1 = w_index * n_delay
                w2 = (w_index + 1) * n_delay

                # Last window could be smaller than others
                width = tf.minimum(signal_len - w2, n_delay)

                new_vals = layer[w2 : w2 + width] + r_decay * layer[w1 : w1 + width]
                layer = tf.concat([layer[:w2], new_vals, layer[w2 + width :]], axis=0)

            result += layer

        signal = normalize(result, dbfs=orig_dbfs)
        return signal

    # Split up into single examples
    signal = tf.map_fn(reverb_helper, signal)

    return signal


# ==================================================================================================


def random_pitch(
    spectrogram, mean: float, stddev: float, cut_min: float, cut_max: float
):
    """Apply random pitch changes, using clipped normal distribution.
    Inspired by: DeepSpeech/training/deepspeech_training/util/augmentations.py"""

    def pitch_helper(spectrogram):
        # Add batch axis
        spectrogram = tf.expand_dims(spectrogram, axis=0)

        # Get a random pitch and clip it
        pitch = tf.random.normal(shape=[], mean=mean, stddev=stddev)
        pitch = tf.minimum(tf.maximum(pitch, cut_min), cut_max)

        # Calculate the new number of frequency bins
        old_shape = tf.shape(spectrogram)
        old_freq_size = tf.cast(old_shape[-1], tf.float32)
        new_freq_size = tf.cast(old_freq_size * pitch, tf.int32)

        # Adding a temporary channel dimension for resizing
        spectrogram = tf.expand_dims(spectrogram, -1)
        spectrogram = tf.image.resize(spectrogram, [old_shape[1], new_freq_size])

        # Crop or pad that we get same number of bins as before
        if pitch > 1:
            spectrogram = tf.image.crop_to_bounding_box(
                spectrogram,
                offset_height=0,
                offset_width=0,
                target_height=old_shape[1],
                target_width=old_shape[2],
            )
        elif pitch < 1:
            spectrogram = tf.image.pad_to_bounding_box(
                spectrogram,
                offset_height=0,
                offset_width=0,
                target_height=old_shape[1],
                target_width=old_shape[2],
            )

        spectrogram = tf.squeeze(spectrogram, axis=-1)
        spectrogram = tf.squeeze(spectrogram, axis=0)
        return spectrogram

    # Split up into single examples, because resize doesn't support different sizes
    spectrogram = tf.map_fn(pitch_helper, spectrogram)

    return spectrogram


# ==================================================================================================


def random_speed(
    spectrogram, mean: float, stddev: float, cut_min: float, cut_max: float
):
    """Apply random speed changes, using clipped normal distribution.
    Transforming the spectrogram is much faster than transforming the audio signal.
    Inspired by: DeepSpeech/training/deepspeech_training/util/augmentations.py"""

    # Get a random speed and clip it, that we don't reach edge cases where the speed is negative
    # Use a single value for all examples in the same batch that they keep the same shape
    change = tf.random.normal(shape=[], mean=mean, stddev=stddev)
    change = tf.minimum(tf.maximum(change, cut_min), cut_max)

    old_shape = tf.shape(spectrogram)
    old_time_size = tf.cast(old_shape[1], tf.float32)
    new_time_size = tf.cast(old_time_size / change, tf.int32)

    # Adding a temporary channel dimension for resizing
    spectrogram = tf.expand_dims(spectrogram, -1)
    spectrogram = tf.image.resize(spectrogram, [new_time_size, old_shape[2]])
    spectrogram = tf.squeeze(spectrogram, axis=-1)

    return spectrogram


# ==================================================================================================


def freq_mask(spectrogram, n=2, max_size=27):
    """See SpecAugment paper - Frequency Masking. Input shape: [nbatch, steps_time, num_bins]
    Taken from: https://www.tensorflow.org/io/api_docs/python/tfio/experimental/audio/freq_mask"""

    nbatch = tf.shape(spectrogram)[0]
    max_freq = tf.shape(spectrogram)[-1]

    for _ in range(n):
        # Using a different size for each batch sample didn't work
        size = tf.random.uniform(shape=[], maxval=max_size, dtype=tf.int32)

        start_mask = tf.random.uniform(
            shape=[nbatch], maxval=max_freq - size, dtype=tf.int32
        )
        start_mask = tf.reshape(start_mask, (-1, 1, 1))
        end_mask = start_mask + size

        indices = tf.reshape(tf.range(max_freq), (1, 1, -1))
        indices = tf.repeat(indices, repeats=nbatch, axis=0)
        condition = tf.math.logical_and(
            tf.math.greater_equal(indices, start_mask), tf.math.less(indices, end_mask)
        )
        spectrogram = tf.where(condition, tf.cast(0, spectrogram.dtype), spectrogram)

    return spectrogram


# ==================================================================================================


def time_mask(spectrogram, n=2, max_size=100):
    """See SpecAugment paper - Time Masking. Input shape: [nbatch, steps_time, num_bins]
    Taken from: https://www.tensorflow.org/io/api_docs/python/tfio/experimental/audio/time_mask"""

    nbatch = tf.shape(spectrogram)[0]
    max_time = tf.shape(spectrogram)[-2]

    for _ in range(n):
        # Using a different size for each batch sample didn't work
        size = tf.random.uniform(shape=[], maxval=max_size, dtype=tf.int32)

        start_mask = tf.random.uniform(
            shape=[nbatch], maxval=max_time - size, dtype=tf.int32
        )
        start_mask = tf.reshape(start_mask, (-1, 1, 1))
        end_mask = start_mask + size

        indices = tf.reshape(tf.range(max_time), (1, -1, 1))
        indices = tf.repeat(indices, repeats=nbatch, axis=0)
        condition = tf.math.logical_and(
            tf.math.greater_equal(indices, start_mask), tf.math.less(indices, end_mask)
        )
        spectrogram = tf.where(condition, tf.cast(0, spectrogram.dtype), spectrogram)

    return spectrogram


# ==================================================================================================


def spec_cutout(spectrogram, n=5, max_freq_size=27, max_time_size=100):
    """Cut out random patches of the spectrogram"""

    def cutout_helper(spectrogram):

        # Temporarily add extra batch and channel dimensions and switch W and H
        spectrogram = tf.expand_dims(spectrogram, axis=0)
        spectrogram = tf.expand_dims(spectrogram, axis=-1)
        spectrogram = tf.transpose(spectrogram, perm=[0, 2, 1, 3])

        width = tf.random.uniform(shape=[], maxval=max_time_size, dtype=tf.int32)
        height = tf.random.uniform(shape=[], maxval=max_freq_size, dtype=tf.int32)

        # Make the random size divisible by 2
        width = tf.cast(width / 2, dtype=tf.int32) * 2
        height = tf.cast(height / 2, dtype=tf.int32) * 2

        for _ in range(n):
            spectrogram = tfa.image.random_cutout(
                spectrogram,
                mask_size=[height, width],
                constant_values=0,
            )

        spectrogram = tf.transpose(spectrogram, perm=[0, 2, 1, 3])
        spectrogram = tf.squeeze(spectrogram, axis=-1)
        spectrogram = tf.squeeze(spectrogram, axis=0)
        return spectrogram

    # Split up into single examples, because cutout doesn't support different sizes
    spectrogram = tf.map_fn(cutout_helper, spectrogram)

    return spectrogram


# ==================================================================================================


def spec_dropout(spectrogram, max_rate=0.1):
    """Drops random values of the spectrogram"""

    nbatch = tf.shape(spectrogram)[0]
    rate = tf.random.uniform(shape=[nbatch], maxval=max_rate, dtype=tf.float32)
    rate = tf.reshape(rate, (-1, 1, 1))

    distrib = tf.random.uniform(
        tf.shape(spectrogram), minval=0.0, maxval=1.0, dtype=tf.float32
    )
    mask = 1 - tf.math.floor(distrib + rate)

    spectrogram = spectrogram * mask
    return spectrogram


# ==================================================================================================


def random_multiply(features, mean: float, stddev: float):
    """Add multiplicative random noise to features"""

    features *= tf.random.normal(shape=tf.shape(features), mean=mean, stddev=stddev)
    return features


# ==================================================================================================


def per_feature_norm(features):
    """Normalize features per channel/frequency"""
    f_mean = tf.math.reduce_mean(features, axis=1)
    f_mean = tf.expand_dims(f_mean, axis=1)

    # Calculate our own std, because pytorch uses unbiasing,
    # which is approximated by reducing the sample number by one
    f_std = features - f_mean
    f_std = tf.reduce_sum(f_std**2, axis=1)
    n = tf.cast(tf.shape(features)[1], dtype=f_std.dtype) - 1.0
    f_std = tf.sqrt(f_std / n)
    f_std = tf.expand_dims(f_std, axis=1)

    zero_guard = 1e-5
    features = (features - f_mean) / (f_std + zero_guard)
    return features
