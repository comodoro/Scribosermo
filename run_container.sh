#! /bin/bash

xhost +; \
docker run --privileged --rm --network host -it \
  --gpus all --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 \
  --volume "$(pwd)"/Scribosermo/:/Scribosermo/ \
  --volume "$(pwd)"/corcua/:/corcua/ \
  --volume "$(pwd)"/checkpoints/:/checkpoints/ \
  --volume "$(pwd)"/data_original/:/data_original/ \
  --volume "$(pwd)"/data_prepared/:/data_prepared/ \
  --volume /tmp/.X11-unix:/tmp/.X11-unix \
  --env DISPLAY --env QT_X11_NO_MITSHM=1 \
  scribosermo
