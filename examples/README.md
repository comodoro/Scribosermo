# Inference Examples

Basic examples of how to use the trained and exported models.

If installing the full _tensorflow_ library is no problem, it is recommended to use the _.pb_ models,
they can be executed on GPU and are much faster on CPU as well.
For single-board computers or other devices with hardware restrictions use the _tflite_ models.
Depending on the device, the _quantized_ models can be faster than the _full_ models, but not always.

Edit the files to your needs:

```bash
# Optionally disable the gpu
export CUDA_VISIBLE_DEVICES=""

# File based inference
python3 /Scribosermo/examples/using_pb.py
python3 /Scribosermo/examples/using_tflite.py

# Streaming. If an audio file is already existing, or for very short audio streams, the file-based transcription approach is recommended
python3 /Scribosermo/examples/using_stream.py
```
