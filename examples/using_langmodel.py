import json
import multiprocessing as mp

import numpy as np
import soundfile as sf
import tflite_runtime.interpreter as tflite
from pyctcdecode import build_ctcdecoder

# ==================================================================================================

checkpoint_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_file = checkpoint_dir + "model_float32.tflite"

alphabet_path = checkpoint_dir + "alphabet.json"
langmodel_path = "/data_prepared/langmodel/en/lm_50M.bin"
vocab_path = "/data_prepared/langmodel/en/vocab_50M.txt"
test_wav_path = "/Scribosermo/exporting/data/test_en.wav"
labels_path = "/Scribosermo/exporting/data/labels.json"

with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)
    alphabet = [a.replace(" ", "▁") for a in alphabet]

with open(labels_path, "r", encoding="utf-8") as file:
    labels = json.load(file)

with open(vocab_path, "r", encoding="utf-8") as file:
    vocab = file.readlines()
    vocab = [v.strip() for v in vocab]


# ==================================================================================================


def load_audio(wav_path):
    """Load wav file with the required format"""

    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def print_prediction(interpreter, decoder, wav_path):

    audio = load_audio(wav_path)
    prediction = predict(interpreter, audio)
    text = decoder.decode(prediction[0])
    print("Prediction with lm: {}".format(text))


# ==================================================================================================


def main():

    # Prepare decoder
    print("\nLoading decoder ...")
    decoder = build_ctcdecoder(
        alphabet,
        kenlm_model_path=langmodel_path,
        unigrams=vocab,
        alpha=0.93,
        beta=1.18,
    )

    # Load model and print some infos about it
    print("\nLoading model ...")
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )
    print("Input details:", interpreter.get_input_details())

    print("\nTranscribing the audio ...")

    # Print label if one of the test wavs is used
    if test_wav_path.startswith("/Scribosermo/exporting/data/test_"):
        lang = test_wav_path[-6:-4]
        if lang in labels:
            print("Label:             ", labels[lang])

    # Now run the transcription
    print_prediction(interpreter, decoder, test_wav_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
